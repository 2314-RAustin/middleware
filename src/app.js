import express from 'express';
import morgan from 'morgan';
import pkg from '../package.json';
import productsRoutes from './routes/products.routes';
import authRoutes from './routes/auth.routes';
import usersRoutes from './routes/users.routes';
import company from './routes/company.routes';
import {createRoles} from './libs/initialSetup'
import cors from 'cors'

const app = express();
createRoles();

app.use(
    cors({
        origin: (origin, callback) => {
            callback(null, true)
        },
        methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
        exposedHeaders: ['Content-Disposition'],
        credentials: true,
    })
);
app.set('pkg',pkg);
app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
    res.json({
        name: app.get('pkg').name,
        author: app.get('pkg').author,
        description: app.get('pkg').description,
        version: app.get('pkg').version,
        license: app.get('pkg').license,
    })
})

app.use('/api/company', company);
app.use('/api/products', productsRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/users', usersRoutes);

export default app;