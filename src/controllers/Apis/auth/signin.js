import jwt from 'jsonwebtoken';
import Logger from '../../../libs/logger'
import User from '../../../models/Users';
import config from '../../../config';
import Binnacle from '../../binnacle'

export default async (req, res) => {

    const logger = new Logger('signin ', 'signin', 'Obtiene data para iniciar sesion');
    logger.info('Bitacorizando data de entrada...');
    const binnacle = new Binnacle();
    await binnacle.input(req.body);

    if(req.body.user){
        if(req.body.password){
            const {user, password} = req.body;
            let userFound = await User.findOne({email:user}).populate("roles") || await User.findOne({username:user}).populate("roles");
            if(userFound){
                console.log('userFound[password] ', userFound['password'])
                if(await User.comparePassword(password, userFound['password'])){
                    const token = jwt.sign({id: userFound._id}, config.SECRET, {
                        expiresIn: 86400//24hrs
                    });
                    logger.succes('Usuario encontrado con contraseña correcta');
                    logger.succes('exito, token expira en 24hrs');
                    logger.info('Bitacorizando datos de salida');
                    binnacle.response(res, 201, 'exito, token expira en 24hrs', {token,userFound}, false);
                } else {
                    logger.error('El password no coincide con el username');
                    logger.info('Bitacorizando datos de salida');
                    binnacle.response(res, 401, "Invalid password", {}, true);
                }
            } else {
                logger.error('No se encontro username');
                logger.info('Bitacorizando datos de salida');
                binnacle.response(res, 400, "user not found", {}, true);
            }
        } else {
            logger.error('No se pudo obtener req.body.password');
            logger.info('Bitacorizando datos de salida');
            binnacle.response(res, 400, "No se pudo obtener req.body.password", {}, true);
        }
    } else {
        logger.error('No se pudo obtener req.body.user');
        logger.info('Bitacorizando datos de salida');
        binnacle.response(res, 400, "No se pudo obtener req.body.user", {}, true);
    }
}