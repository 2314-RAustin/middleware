import User from '../../../models/Users';
import Roles from '../../../models/Roles';
import config from '../../../config';
import jwt from 'jsonwebtoken';

export default async (req, res) => {
    const {username, email, password, roles} = req.body;
     const newUser = new User({
         username,
         email,
         password: await User.encryptPassword(password)
     });
 
     if(roles){
         const foundRoles = await Roles.find({name: {$in: roles}})
         newUser.roles = foundRoles.map(rol => rol._id)
     } else {
         const rol = await Roles.findOne({name: "user"}); 
         newUser.roles = [rol._id];
     }
 
     const savedUser = await newUser.save()
     const token = jwt.sign({id: savedUser._id}, config.SECRET, {
        expiresIn: 86400 //24hrs
     });
 
    res.status(201).json({token, savedUser});
}