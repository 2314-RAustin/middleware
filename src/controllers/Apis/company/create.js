import axios from 'axios';
import Logger from '.././../../libs/logger';

export const index = async (req, res) => {

    console.log('req.body ', req.body)
    if(req.body){
        const newCompany = req.body;
        if('empresa' in newCompany){
            const {empresa} = newCompany;
            let resp = await axios.get('http://localhost:1000/company', {
                data:{
                    query:[{
                        type:'equals',
                        fields:[{
                            empresa
                        }]
                    }]
                }
            })

            if(resp.data){
                const {response} = resp.data;
                if(response && response.length > 0 && response[0].id_company){
                    if(response[0].active){
                        res.status(400).json({
                            error: true,
                            message: `La empresa ${response[0].empresa} ya existe`,
                            response: ''
                        });
                    } else {
                        res.status(400).json({
                            error: true,
                            message: `La empresa ${response[0].empresa} ya existe, sin embargo esta dada de bajo`,
                            response: ''
                        });
                    }

                } else {
                    axios.post('http://localhost:1000/company', newCompany)
                    .then(({data}) => {
                        res.status(200).json({
                            error: false,
                            message: `La empresa ${newCompany.empresa} fue creada exitosamente`,
                            response: data
                        });
                    })
                    .catch(err => {
                        console.log('Error ', err)
                        res.status(400).json({
                            error: true,
                            message: `Error en el servicio`,
                            response: err
                        });
                    })
                }
            }
        }
    }
}