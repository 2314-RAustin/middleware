import axios from 'axios';
export const index = (req, res) => {
    console.log('req.body ', req.body)
    if(req.body){
        const {id_company, empresa} = req.body;
        axios.delete('http://localhost:1000/company', {data: {id_company}})
        .then(({data}) => {
            res.status(200).json({
                error: false,
                message: `La empresa ${empresa} se elimino exitosamente`,
                response: data
            });
        })
        .catch(err => {
            console.log('Error ', err)
            res.status(400).json({
                error: true,
                message: `Error en el servicio`,
                response: err
            });
        })
    }
}