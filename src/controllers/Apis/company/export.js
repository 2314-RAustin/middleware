import axios from 'axios';
import { export_csv, export_xslx } from '../../../helpers/download';

export const all_csv = async (req, res) => {
    let resp = await axios.get('http://localhost:1000/company')
    const headers = [
        { header: 'ID Company', key: 'id_company', width: 10 },
        { header: 'Empresa', key: 'empresa', width: 10 },
        { header: 'Telefono', key: 'telefono', width: 10 },
        { header: 'Contacto', key: 'contacto', width: 10 },
        { header: 'Correo electronico', key: 'correo_electronico', width: 10 },
        { header: 'Giro', key: 'giro', width: 10 },
        { header: 'Convenio', key: 'convenio', width: 10 },
        { header: 'Comentario', key: 'comentarios', width: 10 },
        { header: 'Activo', key: 'active', width: 10 },
    ]

    export_csv(headers, resp.data.response, res)
}

export const all_pdf = async (req, res) => {
    let resp = await axios.get('http://localhost:1000/company')
}

export const all_xslx = async (req, res) => {
    let resp = await axios.get('http://localhost:1000/company')
    const headers = [
        { header: 'ID Company', key: 'id_company', width: 10 },
        { header: 'Empresa', key: 'empresa', width: 10 },
        { header: 'Telefono', key: 'telefono', width: 10 },
        { header: 'Contacto', key: 'contacto', width: 10 },
        { header: 'Correo electronico', key: 'correo_electronico', width: 10 },
        { header: 'Giro', key: 'giro', width: 10 },
        { header: 'Convenio', key: 'convenio', width: 10 },
        { header: 'Comentario', key: 'comentarios', width: 10 },
        { header: 'Activo', key: 'active', width: 10 },
    ]

    export_xslx(headers, resp.data.response, res)
}

export const filter_csv = async (req, res) => {
    let resp = await axios.get('http://localhost:1000/company', {data: {query:{...req.query}}})
    const headers = [
        { header: 'ID Company', key: 'id_company', width: 10 },
        { header: 'Empresa', key: 'empresa', width: 10 },
        { header: 'Telefono', key: 'telefono', width: 10 },
        { header: 'Contacto', key: 'contacto', width: 10 },
        { header: 'Correo electronico', key: 'correo_electronico', width: 10 },
        { header: 'Giro', key: 'giro', width: 10 },
        { header: 'Convenio', key: 'convenio', width: 10 },
        { header: 'Comentario', key: 'comentarios', width: 10 },
        { header: 'Activo', key: 'active', width: 10 },
    ]

    export_csv(headers, resp.data.response, res)
}

export const filter_pdf = async (req, res) => {
    if(req.body){
        const update_company = req.body;
        axios.put('http://localhost:1000/company', update_company)
        .then(({data}) => {
            res.status(200).json({
                error: false,
                message: `La empresa ${update_company.empresa} se actualizo exitosamente`,
                response: data
            });
        })
        .catch(err => {
            console.log('Error ', err)
            res.status(400).json({
                error: true,
                message: `Error en el servicio`,
                response: err
            });
        })
    }
}

export const filter_xslx = async (req, res) => {
    let resp = await axios.get('http://localhost:1000/company', {data: {query:{...req.query}}})
    const headers = [
        { header: 'ID Company', key: 'id_company', width: 10 },
        { header: 'Empresa', key: 'empresa', width: 10 },
        { header: 'Telefono', key: 'telefono', width: 10 },
        { header: 'Contacto', key: 'contacto', width: 10 },
        { header: 'Correo electronico', key: 'correo_electronico', width: 10 },
        { header: 'Giro', key: 'giro', width: 10 },
        { header: 'Convenio', key: 'convenio', width: 10 },
        { header: 'Comentario', key: 'comentarios', width: 10 },
        { header: 'Activo', key: 'active', width: 10 },
    ]

    export_xslx(headers, resp.data.response, res)
}