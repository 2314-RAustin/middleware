import axios from 'axios';
export const index = async (req, res) => {
    if(req.query){
        let resp = await axios.get('http://localhost:1000/company', {data: {query:{...req.query}}})
        res.status(200).json(resp.data);

    } else {
        let resp = await axios.get('http://localhost:1000/company')
        res.status(200).json(resp.data);
    }
}