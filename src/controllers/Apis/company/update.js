import axios from 'axios';
export const index = (req, res) => {
    if(req.body){
        const update_company = req.body;
        axios.put('http://localhost:1000/company', update_company)
        .then(({data}) => {
            res.status(200).json({
                error: false,
                message: `La empresa ${update_company.empresa} se actualizo exitosamente`,
                response: data
            });
        })
        .catch(err => {
            console.log('Error ', err)
            res.status(400).json({
                error: true,
                message: `Error en el servicio`,
                response: err
            });
        })
    }
}