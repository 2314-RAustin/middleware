import Product from '../../../models/Product';

export const createProduct = async (req, res) => {
    const productSaved = new Product(req.body)
    res.status(201).json(await productSaved.save());
}