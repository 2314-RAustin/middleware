import Product from '../../../models/Product';

export const deleteProductById = async (req, res) => {
    res.status(204).json(await Product.findByIdAndDelete(req.params.productId));
}