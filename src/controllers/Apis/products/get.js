import Product from '../../../models/Product';

export const getProducts = async (req, res) => {
    res.status(200).json(await Product.find());
}

export const getProductById = async (req, res) => {
    res.status(200).json(await Product.findById(req.params.productId));
}