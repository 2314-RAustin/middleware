import Product from '../../../models/Product';

export const updateProductById = async (req, res) => {
    res.status(200).json(await Product.findByIdAndUpdate(req.params.productId, req.body, {new: true}));
}