import User from '../../../models/Users';

export const createUser = async (req, res) => {
    const {username, email, password, roles} = req.body;
    const newUser = new User({
        username,
        email,
        password: await User.encryptPassword(password)
    });
    const savedUser = await newUser.save()
    res.status(201).json({savedUser});
}