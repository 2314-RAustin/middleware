import moment from 'moment-timezone'

import Input from './input'
import Output from './output'

export default class Binnacle
{
    _timeInital = "";
    _timeFinish = '';
    _input = {};
    _output = {};
    _object_id = "";

    async input(objeto)
    {
        this._input = objeto
        this._timeInital = moment(new Date()).tz('America/Tijuana').format('YYYY-MM-DD HH:mm:ss');
        const inp = new Input(this._input, this._timeInital);
        const {error, response} = await inp.main();

        if(!error)
        {
            this._object_id = response['_id']
        }
        else
        {
            console.log("Error============ ", response)
        }
    }

    output(objeto)
    {
        this._output = objeto;
        this._timeFinish = moment(new Date()).tz('America/Tijuana').format('YYYY-MM-DD HH:mm:ss');
        const out = new Output(this._output, this._timeFinish, this._object_id);
        out.main();
    }

    response(res, code, message, response, error)
    {
        this.output({code, message, response, error});
        res.status(code).json(this._output);
    }
}