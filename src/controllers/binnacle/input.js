import Binnacle from '../../models/Binnacle'

export default class Input 
{
    objeto = {}
    timeStart = '';
    constructor(objeto, time){
        this.objeto = objeto;
        this.timeStart = time;
    }

    main()
    {
        return new Promise(async (ready) => {
            const BinnacleSaved = await new Binnacle({
                input: this.objeto,
                timeStart:this.timeStart,
            })
            
            await BinnacleSaved.save()
            .then(resp => {
                ready({
                    error: false,
                    response: resp
                })
            })
            .catch(error => {
                ready({
                    error: true,
                    response: error
                })
            })
        })
    }
}