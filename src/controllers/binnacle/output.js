import Binnacle from '../../models/Binnacle'

export default class Output 
{
    objeto = {}
    timeEnd = '';
    _object_id = ''

    constructor(objeto, time, _id){
        this.objeto = objeto;
        this.timeEnd = time;
        this._object_id = _id;
    }

    async main()
    {
        if(this._object_id.toString().length > 0)
        {
            await Binnacle.updateOne({_id: this._object_id}, {
                output: JSON.parse(JSON.stringify(this.objeto)),
                timeEnd: this.timeEnd
            })
            
        }
        else
        {
            console.log('Venia a actualizar pero requiero id')
        }

    }
}