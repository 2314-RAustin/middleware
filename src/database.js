import mongoose from 'mongoose';
import Logger from './libs/logger'

const logger = new Logger('database', 'mongoose.connect', 'connect mongoose database');

mongoose.connect("mongodb://localhost", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true,
    dbName: 'middleware',
})
.then(db => {
    logger.succes('Databse succesfull conected ', db);
})
.catch(err => {
    logger.error('Databse error conected ', err);
})