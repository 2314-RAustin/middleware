import Excel from 'exceljs';

export const export_xslx = (headers, data, response) => {
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('My Sheet');
    worksheet.columns = headers;
    for (const row of data) {
        worksheet.addRow(row);
    }

    export_fiile(workbook, 'xlsx', response)
}

export const export_csv = (headers, data, response) => {
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('My Sheet');
    worksheet.columns = headers;
    for (const row of data) {
        worksheet.addRow(row);
    }

    export_fiile(workbook, 'csv', response)
}

const export_fiile = (workbook, type, response) => {
    workbook[type].writeFile(`./temp.${type}`).then(function() {
        var fileName = `FileName.${type}`;
        response.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        workbook[type].write(response).then(function(){
            response.end();
        });
    });
}