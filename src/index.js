import app from './app'
import Logger from './libs/logger';
import './database';

const port = 2000;
app.set('port', port)
app.listen(port);

const logger = new Logger('index', 'index', 'listen on port from server');
logger.succes('Server listen on port ', port);