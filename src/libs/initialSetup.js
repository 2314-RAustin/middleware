import Roles from '../models/Roles';
import Logger from './logger'

export const createRoles = async () => {
    const logger = new Logger('initalSetup','createRoles','Crea los roles si no existen en base de datos por primera vez');

    if (await Roles.estimatedDocumentCount() > 0) return;

    Promise.all([
        new Roles({name: "user"}).save(),
        new Roles({name: "admin"}).save(),
        new Roles({name: "moderator"}).save()
    ])
    .then(resp => {
        logger.succes('Se crearon los roles exitosamente resp: ', resp);
    })
    .catch(err => {
        logger.succes('Error al crear roles: ', err);
    })


}