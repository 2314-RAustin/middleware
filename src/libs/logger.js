import fs from 'fs';
import moment from 'moment'

export default class Logger
{
	clase = '';
	func = '';
	bussinessLogic = '';
	timeStart = '';
	timeEnd = '';
	timeExcecuted = '';
	timeExcecutedSeconds = '';

	constructor(clase = '', func = '', bussinessLogic = '')
	{
		this.timeStart = Date.now();
		this.clase = `Clase: ${clase}`;
		this.func = `Function: ${func}`;
		this.bussinessLogic = `Bussiness logic: ${bussinessLogic}`;

		this.print();
	}

	 getLogFull(...logs){
		let logFull = '';
		for (let i = 0; i < logs.length; i++) {
			for (let j = 0; j < logs[i].length; j++) {
				logFull += logs[i][j].toString();
			}
		}
		return logFull;
	}

	fatherLogs(typeLog, color, msgExtra, log)
	{
		Logger.print(typeLog, `${color} ${moment().toDate()} ${msgExtra} ${log}`);
	}
	
	error(...logs)
	{
		this.fatherLogs('error', '🔴', 'Error:', this.getLogFull(logs));
	}
	
	warning(...logs)
	{
		this.fatherLogs('warning', '🟠', 'Warning:', this.getLogFull(logs));
	}
	
	info(...logs)
	{
		this.fatherLogs('info', '🔵', 'Info:', this.getLogFull(logs));
	}
	
	succes(...logs)
	{
		this.fatherLogs('succes', '🟢', 'Succes:', this.getLogFull(logs));
	}

	time()
	{
		this.timeEnd = Date.now();
		this.timeExcecuted = this.timeEnd - this.timeStart;
		this.timeExcecutedSeconds = (this.timeEnd / 1000) - (this.timeStart / 1000);
		Logger.print("info", `Hour init ${this.timeStart}`);
		Logger.print("info", `Hour end ${this.timeEnd}`);
		Logger.print("info", `Time excecuted ${this.timeExcecuted} milliseconds`);
		Logger.print("info", `Time excecuted ${this.timeExcecutedSeconds} seconds`);
	}

	print()
	{
		Logger.print('', '=================================================================');
		Logger.print('', this.clase);
		Logger.print('', this.func);
		Logger.print('', this.bussinessLogic);
	}

	static print(typeLog = '', log = '')
	{
		switch (typeLog) 
		{
			case 'error':
				console.error(log)
				break;
			case 'info':
				console.info(log)
				break;
			case 'succes':
				console.log(log)
				break;
			case 'warning':
				console.warn(log)
				break;
			default:
				console.log(log)
				break;
		}

		Logger.record(log);
	}

	static record(log, timestamp)
	{
		let today = new Date();
		let nameFileDate = `${today.getDate()}.${today.getMonth()+1}.${today.getFullYear()}`;

		fs.appendFile(`./src/storage/logs/${nameFileDate} - console.log`, log + '\n', 
		function(err)
		{
			if(err)
			{
		        return console.error(err);
		    }
		}); 
	}
}
