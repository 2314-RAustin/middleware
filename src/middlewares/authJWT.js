import jwt from 'jsonwebtoken';
import config from '../config';
import Users from '../models/Users';
import Roles from '../models/Roles';

export const verifyToken = async (req, res, next) => {
    try {
        const token = req.headers['x-access-token'];
        if(token){
            const decoded = jwt.verify(token, config.SECRET);
            if(decoded){
                req.userId = decoded.id;
                if(req.userId){
                    const user = await Users.findById(req.userId, {password: 0});
                    if(user){
                        next();
                    } else {
                        return res.status(404).json({module: 'verifyToken', message:'No user found'})
                    }
                } else {
                    return res.status(404).json({module: 'verifyToken', message:'No user found'})
                }
            } else{
                return res.status(403).json({module: 'verifyToken', message:'No token provided'})
            }
        } else {
            return res.status(403).json({module: 'verifyToken', message:'No token provided'})
        }
    } catch (error) {
        return res.status(500).json({module: 'verifyToken', message:'Unauthorized', error})
    }
    
}

export const isModerator = async (req, res, next) => {
    try {
        if(req.userId){
            console.log(req.userId)
            const user = await Users.findById(req.userId);
            if(user){
                const roles = await Roles.find({_id: {$in: user.roles}});
                if(roles && Array.isArray(roles)){
                    const hasRoleModerator = roles.find(o => o.name === "moderator")
                    if(hasRoleModerator){
                        next();
                    } else {
                        return res.status(404).json({module: 'isModerator', message:'The user requires moderator role'})
                    }
                } else {
                    return res.status(404).json({module: 'isModerator', message:'The user dont has rol'})
                }
            } else {
                return res.status(404).json({module: 'isModerator', message:'No user found'})
            }
        } else{
            return res.status(404).json({module: 'isModerator', message:'No user found'})
        }
    } catch (error) {
        return res.status(500).json({module: 'isModerator', message:'Unauthorized',  error: error})
    }
}

export const isAdmin = async (req, res, next) => {
    try {
        if(req.userId){
            const user = await Users.findById(req.userId);
            if(user){
                const roles = await Roles.find({_id: {$in: user.roles}});
                if(roles && Array.isArray(roles)){
                    const hasRoleModerator = roles.find(o => o.name === "admin")
                    if(hasRoleModerator){
                        next();
                    } else {
                        return res.status(404).json({module: 'isAdmin', message:'The user requires admin role'})
                    }
                } else {
                    return res.status(404).json({module: 'isAdmin', message:'The user dont has rol'})
                }
            } else {
                return res.status(404).json({module: 'isAdmin', message:'No user found'})
            }
        } else{
            return res.status(404).json({module: 'isAdmin', message:'No user found'})
        }
    } catch (error) {
        return res.status(500).json({module: 'isAdmin', message:'Unauthorized', error: error})
    }
}