import Roles from '../models/Roles';
import User from '../models/Users'

export const checkDuplicateUsernameOrEmail = async (req, res, next) => {
    if(req.body.username){
        if(req.body.email){
            if(req.body.password){

                const username = await User.findOne({username: req.body.username});
                const email = await User.findOne({email: req.body.email});
                if(!username){
                    if(!email){
                        next();
                    } else {
                        return res.status(400).json({module: 'checkDuplicateUsernameOrEmail', message:'el email ya existe', email: email})
                    }
                } else {
                    return res.status(400).json({module: 'checkDuplicateUsernameOrEmail', message:'el username ya existe', username: username})
                }
            } else {
                return res.status(400).json({module: 'checkDuplicateUsernameOrEmail', message:'Se requiere password'})
            }
        } else {
            return res.status(400).json({module: 'checkDuplicateUsernameOrEmail', message:'Se requiere email'})
        }
    }else{
        return res.status(400).json({module: 'checkDuplicateUsernameOrEmail', message:'Se requiere username'})
    }
}

export const checkRolesExsited = async (req, res, next) => {
    let error = false;
    let rol_error = '';

    if(req.body.roles){
        const rolesRecived = req.body.roles;
        if(rolesRecived && Array.isArray(rolesRecived)){
            const allRoles = await Roles.find();
            if(allRoles){
                for (const rol of rolesRecived) {
                    let matchRol = allRoles.find(allRol => allRol.name === rol)
                    if(matchRol){
                        continue;
                    } else {
                        rol_error = rol;
                        error = true;
                        break;
                    }
                }
                if(!error){
                    next();
                } else {
                    return res.status(500).json({module: 'checkRolesExsited', message:'Uno de los roles no existe', rol: rol_error})
                }
            } else {
                return res.status(500).json({module: 'checkRolesExsited', message:'Error from database'})
            }
        } else {
            return res.status(404).json({module: 'checkRolesExsited', message:'No user found'})
        }
    } else {
        //si no tiene roles la creacion del nuevo usuario por defecto sera user
        next();
    }
}