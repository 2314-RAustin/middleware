import {Schema, model} from 'mongoose';

const BinnacleSchema = new Schema({
    input: Object,
    output: Object,
    timeStart:{type : Date, default: Date.now},
    timeEnd:{type : Date, default: Date.now}
},{
    versionKey: false
});

export default model('Binnacle', BinnacleSchema)