import {Schema, model} from 'mongoose';
import bcript from 'bcryptjs';

const userShema = new Schema({
    username:{
        type: String,
        unique: true
    },
    email:{
        type: String,
        unique: true
    },
    password:{
        type: String,
        unique: true
    },
    roles:[{
        ref: "Roles",
        type:Schema.Types.ObjectId
    }]
},{
    timestamps: true,
    versionKey: false,
});

userShema.statics.encryptPassword = async (password = "") => {
    const salt = await bcript.genSalt(10)
    return await bcript.hash(password, salt);
}

userShema.statics.comparePassword = async (receivedPassword = "", encryptPassword = "") => {
    return await bcript.compare(receivedPassword, encryptPassword);
}

export default model('User', userShema);