import express from 'express';
import auth from '../controllers/Apis/auth'
import {middleware, verifySignUp} from '../middlewares'

const router = express.Router();

router.post('/signin', auth.signin)
router.post('/signup', [verifySignUp.checkRolesExsited, verifySignUp.checkDuplicateUsernameOrEmail], auth.signup)

export default router;