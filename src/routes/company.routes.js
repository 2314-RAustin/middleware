import express from 'express';
import company from '../controllers/Apis/company';
// import {middleware, verifySignUp} from '../middlewares';

const router = express.Router();

router.get('/', company.get.index)
router.post('/', company.create.index)
router.put('/', company.update.index)
router.delete('/', company.delete.index)
router.get('/export/all/csv', company.export.all_csv)
router.get('/export/all/pdf', company.export.all_pdf)
router.get('/export/all/xslx', company.export.all_xslx)
router.get('/export/filter/csv', company.export.filter_csv)
router.get('/export/filter/pdf', company.export.filter_pdf)
router.get('/export/filter/xslx', company.export.filter_xslx)


export default router;