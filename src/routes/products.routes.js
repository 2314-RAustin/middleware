import express from 'express';
import * as productsCtrl from '../controllers/Apis/products';
import products from '../controllers/Apis/products'
import {middleware} from '../middlewares'
const router = express.Router();

router.post('/', [middleware.verifyToken, middleware.isModerator, middleware.isAdmin], products.create.createProduct);
router.get('/', [middleware.verifyToken, middleware.isModerator, middleware.isAdmin], products.get.getProducts);
router.get('/:productId', [middleware.verifyToken, middleware.isModerator, middleware.isAdmin], products.get.getProductById);
router.put('/:productId', [middleware.verifyToken, middleware.isModerator, middleware.isAdmin], products.update.updateProductById);
router.delete('/:productId', [middleware.verifyToken, middleware.isModerator, middleware.isAdmin], products.delete.deleteProductById);

export default router;