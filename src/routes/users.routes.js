import express from 'express';
import * as usersCtls from '../controllers/Apis/users/create';
import {middleware, verifySignUp} from '../middlewares'

const router = express.Router();

router.post('/', [
    middleware.verifyToken,
    verifySignUp.checkRolesExsited,
    verifySignUp.checkDuplicateUsernameOrEmail,
    middleware.isModerator,
    middleware.isAdmin
],
    usersCtls.createUser
);

export default router;